﻿using System;
using System.IO;
using System.Net.Http;
using System.Text.RegularExpressions;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace ElasticDelete
{
    public class Program
    {
        private static Settings Settings { get; } = new DeserializerBuilder().WithNamingConvention(new CamelCaseNamingConvention()).Build().Deserialize<Settings>(File.ReadAllText("Settings.yaml"));

        public static void Main(string[] args)
        {
            if (args.Length == 1 || args.Length == 2 && args[0] == "pattern")
            {
                var firstArg = args[0];

                if (firstArg == "today")
                    DeleteIndexFor(DateTime.Now);
                else if (Regex.IsMatch(firstArg, @"^-\d+d$"))
                    DeleteIndexFor(DateTime.Now - TimeSpan.FromDays(double.Parse(firstArg.Substring(1, firstArg.Length - 2))));
                else if (Regex.IsMatch(firstArg, @"^\d{4}/\d{2}/\d{2}"))
                    DeleteIndexFor(DateTime.Parse(firstArg));
                else if (firstArg == "pattern")
                    DeleteIndexFor(args[1]);
                else
                    PrintUsage();
            }
            else
            {
                PrintUsage();
            }
        }

        private static void PrintUsage()
        {
            Console.WriteLine("Usage: elasticdelete today|-<n>d|yyyy/mm/dd|pattern <pattern>");
            Console.WriteLine("Examples:");
            Console.WriteLine("'elasticdelete today' to delete today's index");
            Console.WriteLine("'elasticdelete -4d' to delete the index for the day 4 days ago");
            Console.WriteLine("'elasticdelete 2016/12/25' to delete the index for the day on the 25th of December 2016");
            Console.WriteLine("'elasticdelete pattern logstash-*' to delete the indexes matching the pattern 'logstash-*'");
        }

        private static void DeleteIndexFor(DateTime date)
        {
            var index = string.Format(Settings.Pattern, date);

            DeleteIndexFor(index);
        }

        private static void DeleteIndexFor(string pattern)
        {
            var url = Settings.ElasticsearchAddress.EndsWith("/")
                ? $"http://{Settings.ElasticsearchAddress}{pattern}"
                : $"http://{Settings.ElasticsearchAddress}/{pattern}";

            Console.WriteLine($"Sending delete request: {url}");

            var http = new HttpClient();
            http.DeleteAsync(url).Wait();

            Console.WriteLine("Done!");
        }
    }
}
