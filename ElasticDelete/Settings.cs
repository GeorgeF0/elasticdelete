﻿namespace ElasticDelete
{
    public class Settings
    {
        public string ElasticsearchAddress { get; set; }
        public string Pattern { get; set; }
    }
}
